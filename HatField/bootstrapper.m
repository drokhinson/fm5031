function [ZCB Spots] = bootstrapper(mats,paryields)
%This funciton will bootrap a par yield curve and output a set of ZCB
%semi-annually out to the last maturity and also the corresponding continously
% compounded spot rates
%The method assumes contant forwards between known mats
%
mats = mats(:);
paryields = paryields(:);


ZCB = ones(1,2*mats(end)+1);

prevtime = 0;
Zknown = 1;
for k =  1:length(mats)

coupon = paryields(k) ;   
rateguess = coupon;


time = mats(k);

% find the frate that makes the swap have zero value

frate = fzero(@(fr) swap(fr,coupon, prevtime, time, Zknown),rateguess);


ftimes1 = [(prevtime+.5):.5:time];
newzcbs1 = exp(-frate*(ftimes1 - prevtime))*Zknown(end);

Zknown = [Zknown newzcbs1];
prevtime = time;
end

ZCB = Zknown(2:end);

semitimes = [.5:.5:mats(end)];
Spots = -log(ZCB)./semitimes;   %Doing it here.  Always an easy step if not done here



function   [swap] = swap(frate,coupon, prevtime, time, Zknown)

%Calculate the value of the swap for a test forward rate



ftimes = [(prevtime +.5):.5:(time)];
newzcbs = exp(-frate*(ftimes-prevtime))*Zknown(end);

newzcbs = [Zknown newzcbs];

cfs = ones(1,length(newzcbs))* coupon/2;
cfs(1) = -1;
cfs(end) = 1+ cfs(end);

swap = cfs*newzcbs';


end
    

end





