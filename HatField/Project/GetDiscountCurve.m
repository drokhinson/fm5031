function disc = GetDiscountCurve(spotRates, spotMats)
    disc = exp(-spotRates.*spotMats);
end