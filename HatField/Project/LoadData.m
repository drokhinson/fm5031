liabilities = csvread(fullfile(pwd, 'Liabs.csv'), 1, 0);
liabilityMats = liabilities(:,1)
liabilityPmts = liabilities(:,2) * 1000000

assets = csvread(fullfile(pwd, 'Assets.csv'), 1, 0);
bondValues = assets(:,1)
bondBEYs = assets(:,2)
bondMats = assets(:,3)

swapRates = csvread(fullfile(pwd, 'Markets.csv'), 1, 1) / 100
swapMats = [0.5, [1:1:20], 25, 30]

caps = csvread(fullfile(pwd, 'Caps.csv'), 1, 0);
capMats = caps(:,1)
capVols = caps(:,2)