function [tSeries paths] = GenerateMCScenarios(t, r0, gamma, rBar, sig, numPaths)
    dt = 1/getDt();
    tSeries = 0:dt:t;

    paths = zeros(numPaths, length(tSeries));
%     paths(0:numPaths,:) = VasicekSenario(r0, tSeries, gamma, rBar, sig);
    for p = 1:numPaths
        paths(p,:)=VasicekSecnario(r0, tSeries, gamma, rBar, sig);
    end
end
% MonteCarlo Stuff
function s = getDt()
    s = 100;
end

function s = VasicekSecnario(r0, tSeries, gamma, rBar, sig)
    l = length(tSeries);
    s = [r0 randn(1, l-1)];
    for x = 2:l
        r_t = s(x-1);
        m = gamma*(rBar-r_t);
        dt = tSeries(x)-tSeries(x-1);
        s(x) = r_t+m*dt+sig*sqrt(dt)*s(x);
    end
end