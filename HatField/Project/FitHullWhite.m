%% Fitting Methods
function paramsHW = FitHullWhite(spotRates, spotMats, capVols, capMats)
    discounts = GetDiscountCurve(spotRates, spotMats);
    capPrice = calcCap(capMats(1), capVols(1), spotRates', discounts, spotMats);
    for i = 2:length(capMats)
        capPrice = [capPrice; calcCap(capMats(i), capVols(i), spotRates', discounts, spotMats)];
    end
    capPrice = capPrice';
    J =@(gamma_, sig_) 100*(capPrice - CalcCap_HullWhite(discounts, capMats, gamma_, sig_)) *...
        (capPrice - CalcCap_HullWhite(discounts, capMats,  gamma_, sig_))';
    J1 = @(x) J(x(1),x(2));
    paramsHW = fminsearch(J1,[.1 .1]);
end

function price = calcCap(term, sig_f, spotCurve, discCurve, spotMats)
    frwdCurve = getFwdCurve(discCurve);
    k = 4*(1-discCurve(4*term))/sum(discCurve(1:(4*term)));
    disc = discCurve(1:term*4);
    sig_z = sig_f*sqrt(.25:.25:term);
    d1 = (1./sig_z).*(log(frwdCurve(1:4*term)./k)+sig_z.^2./2);
    d2 = d1 - sig_z;

    caplets = disc.*(frwdCurve(1:4*term).*normcdf(d1)- k.*normcdf(d2));
    price = sum(caplets);
end

%% CashFlow discounting methods
function c = calcCapDR(term, sig_f, spotCurve, discCurve, spotMats)
    N = 1;
    k = 4*(1-discCurve(4*term))/sum(discCurve(1:(4*term)));
    t = getPayoffTimes(term, 0.25);
    tEval = t(1:length(t)-1);
    
    frwdEval = frwdCurve(1:length(t)-1);
    d1 = (1./sig_f./sqrt(tEval)).*log(frwdEval./k)+sig_f.*sqrt(tEval)./2
    d2 = d1-sig_f.*sqrt(tEval)
    
    caplets = N.*0.25.*discCurve(term*4).*(frwdEval.*normcdf(d1)-k.*normcdf(d2));
    c = sum(caplets);
end

function f = getFwdCurve(discCurve)
    f = 4*([1/discCurve(1), discCurve(1:end-1)./discCurve(2:end) ]-1);
end

function t = getPayoffTimes(termArray, compounding)
    t = compounding:compounding:max(termArray);
end

function caps = CalcCap_HullWhite(discCurve,mats, gamma, sig)
    caps = zeros(1,length(mats));
    for j=1:length(mats)
        term = mats(j);
        k = 4*(1-discCurve(4*term))/sum(discCurve(1:(4*term)));
        caps(j) = capHW(k,term,discCurve,gamma, sig); 
    end
end

function [caplet] = capletHW(k, to, zto, ztb, gamma, sigma)
    %Hull White caplet pricer
    B = 1/gamma *(1- exp(-gamma*.25));
    sz= B*sigma/gamma*sqrt((1-exp(-2*gamma*to))/2);
    caplet = (1+k/4)*BlackPut(ztb/zto,1/(1+k/4),sz, zto);
end

%% Hull White Methods
function [cap] = capHW(k,term,discCurve,gamma, sigma)
        optiontimes = [.25:.25:term-.25];
        caplets = zeros(1,length(optiontimes));  
        for j= 1:length(optiontimes)
            caplets(j) =  ...
                capletHW(k, optiontimes(j),discCurve(j), ...
                discCurve(j+1),gamma, sigma );            
        end
        cap = sum(caplets) ;   
end

function [price] = BlackPut(F,K, S_Z,Num)
    % Black's formula
    d1 = (1./S_Z).*(log(F./K)+.5*S_Z.^2);
    d2 = d1 - S_Z;
    price = Num.*(K.*normcdf(-d2)- F.*normcdf(-d1));
end