function [spotRatesQuarterly, quarterlyTimes] = BootstrapQuarterlyRates(termArray, beyArray)
    times = getPayoffTimes(termArray, 0.5);    
    [discount spotRates] = BootstrapRates(termArray,beyArray);
    quarterlyTimes = getPayoffTimes(termArray, 0.25);
    spotRatesQuarterly = interp1([0.25 times], [spotRates(1) spotRates], quarterlyTimes);
end

function [spotRatesQuarterly, quarterlyTimes] = BootstrapQuarterlyRatesDR(termArray, beyArray)
    compounding = 0.5;
    if(min(termArray ~= compounding))
        termArray = [compounding termArray];
        beyArray = [beyArray(1) beyArray];
    end

    times = getPayoffTimes(termArray, compounding);
    beyLinearInterp = interp1(termArray, beyArray, times);
    cf = buildCashFlow(times, beyLinearInterp, compounding);
    par = ones(length(beyLinearInterp), 1) .* 100;
    discount = linsolve(cf, par);
    spotRates = -log(discount)./times';
    [times' discount spotRates]
    
    quarterlyTimes = getPayoffTimes(termArray, 0.25);
    spotRatesQuarterly = interp1([0.25 times], [spotRates(1); spotRates], quarterlyTimes);
end

function t = getPayoffTimes(termArray, compounding)
    t = compounding:compounding:max(termArray);
end

function cf = buildCashFlow(termArray, beyArray, compounding)
    times = getPayoffTimes(termArray,compounding);
    dimT = length(times);
    dimB = length(beyArray);
    par = 100;
    cf = zeros(dimB, dimT);
    for x = 1:dimB
        cf(x,:) = (par*(termArray(x)==times)) + ...
            (par.*beyArray(x)*compounding*(times<=termArray(x)));
    end
end

function [ZCB Spots] = BootstrapRates(mats,paryields)
    mats = mats(:);
    paryields = paryields(:);
    ZCB = ones(1,2*mats(end)+1);

    prevtime = 0;
    Zknown = 1;
    for k =  1:length(mats)
        coupon = paryields(k) ;   
        rateguess = coupon;
        time = mats(k);
        % find the frate that makes the swap have zero value
        frate = fzero(@(fr) swap(fr,coupon, prevtime, time, Zknown),rateguess);
        ftimes1 = [(prevtime+.5):.5:time];
        newzcbs1 = exp(-frate*(ftimes1 - prevtime))*Zknown(end);
        Zknown = [Zknown newzcbs1];
        prevtime = time;
    end
    ZCB = Zknown(2:end);
    semitimes = [.5:.5:mats(end)];
    Spots = -log(ZCB)./semitimes;
    
    function   [swap] = swap(frate,coupon, prevtime, time, Zknown)
        ftimes = [(prevtime +.5):.5:(time)];
        newzcbs = exp(-frate*(ftimes-prevtime))*Zknown(end);
        newzcbs = [Zknown newzcbs];
        
        cfs = ones(1,length(newzcbs))* coupon/2;
        cfs(1) = -1;
        cfs(end) = 1+ cfs(end);

        swap = cfs*newzcbs';
    end
end





