function exposure = GetSwapDuration(frwdSwapRate, term, shock, spotRates, spotMats)
    dUpShock = exp(-(spotRates+shock).*spotMats);
    dDwnShock = exp(-(spotRates-shock).*spotMats);
    valSwap = @(discountCurve, t) (frwdSwapRate /2 * sum(discountCurve(1:2:locTable(t))) - ...
        (1 - discountCurve(locTable(t))));
    exposure = (valSwap(dUpShock, term) - valSwap(dDwnShock, term)) / (shock * 2);
end

function l = locTable(time)
    l = time*4;
end

function [times, spotRates, discount] = calcSpotRate_Linear(termArray, beyArray, compounding)
    if(min(termArray ~= compounding))
        termArray = [compounding termArray];
        beyArray = [beyArray(1) beyArray];
    end
    
    times = getPayoffTimes(termArray, compounding);
    beyLinearInterp = interp1(termArray, beyArray, times);
    cf = buildCashFlow(times, beyLinearInterp, compounding, 100);
    par = ones(length(beyLinearInterp), 1) .* 100;
    discount = linsolve(cf, par);
    spotRates = -log(discount)./times';
end

function fwrdSwapRate = calcForwardSwapRate(t_start, t_end, discountArray)
    v_float =  2 * (discountArray(locTable(t_start)) - discountArray(locTable(t_end)));
    dArray = getSubsetTable(discountArray, 0.5, t_end, t_start+0.5);
    v_fixed = sum(dArray);
    fwrdSwapRate = v_float / v_fixed;
end

function cf = buildCashFlow(termArray, beyArray, compounding, par)
    beyArray = beyArray*compounding;
    times = getPayoffTimes(termArray, compounding);
    dimT = length(times);
    dimB = length(beyArray);
    cf = zeros(dimB, dimT);
    for x = 1:dimB
        cf(x,:) = (par*(termArray(x)==times)) + ...
            (100.*beyArray(x)*(times<=termArray(x)));
    end
end

function t = getPayoffTimes(termArray, comp)
    t = comp:comp:max(termArray);
end