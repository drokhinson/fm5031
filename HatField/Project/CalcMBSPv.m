function MBSpv = CalcMBSPv(paths30, spotRates, spotMats)
    ratePath = mean(paths30,1);
    L0 = 40e6;
    coupon = 0.048 * L0;
    MBScfs = MBSCashFlow(L0, coupon, ratePath(21:21:end));
    spotMatsTemp = [0 spotMats];
    spotRatesTemp = [spotRates(1) spotRates];
    spotRatesMonthly = interp1(spotMatsTemp, spotRatesTemp, 1/12:1/12:30);
    discMonthly = GetDiscountCurve(spotRatesMonthly, 1/12:1/12:30);
    MBSpv = sum(MBScfs.*discMonthly');
end

function [CF, L, paysched, paypre, int] = MBSCashFlow(L0, coupon,sRate)
%UNTITLED2 Summary of this function goes here
%   Will about information about a mortgage pool assumimng the pool is
%   homogenous with a 30 year maturity

%Define CPR
CPR = [linspace(.002, .06, 30), .06*ones(1,330)]';
speed = 1.25*exp(-90*(sRate-0.02));
test = min(1-speed.*CPR',0);
if(sum(test) ~= 0)
    speed = 1;
end
p = 1- (1-speed.*CPR').^(1/12);
p = p';

mrate = calcMortgageRate(L0, coupon, 30, 12); %30yr term, 12payment/yr

  C = coupon* ones(361,1);
  C(2:end) = C(2:end).* cumprod(1-p);
  C = C(1:end-1);
  
  L = L0 * ones(361,1);
  int = zeros(360,1);
  paypre = ones(360,1);
  paysched = ones(360,1);
  rm = mrate/12;
  
  for m = 1:360
      paypre(m) = L(m) * p(m);
      int(m) =  L(m) * rm;
      paysched(m) = C(m) - int(m);
      L(m+1) = L(m) - paysched(m) - paypre(m);
       
  end
  CF = C + paypre;
end

function t = calcTerm(mortgageL, paymentPerYear)
    t = mortgageL * paymentPerYear;
end

function rateAnnual = calcMortgageRate(loanAmt, payment, mortgageL, paymentPerYear)
    term = calcTerm(mortgageL, paymentPerYear);
    fun = @(r) calcPayment(loanAmt, 0, r, term)-payment;
    rateAnnual = fzero(fun, 1) * paymentPerYear;
end

function amt = calcLoanAmt(payment, rateAnnual, mortgageL, paymentPerYear)
    rate = rateAnnual / paymentPerYear;
    term = calcTerm(mortgageL, paymentPerYear);
    fun = @(a) calcPayment(a, 0, rate, term)-payment;
    amt = fzero(fun, 10000);
end

function c = calcPayment(loanAmt, downPayment, rate, term)
    notional = loanAmt - downPayment;
    c = notional.*rate./(1-(1+rate).^-(term));
end