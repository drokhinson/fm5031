function [CF, L, paysched, paypre, int] = MBSCashFlow(L0, coupon,speed,mrate)
%UNTITLED2 Summary of this function goes here
%   Will about information about a mortgage pool assumimng the pool is
%   homogenous with a 30 year maturity

%Define CPR
CPR = [linspace(.002, .06, 30), .06*ones(1,330)]';


p = 1- (1-speed* CPR).^(1/12);

  C = coupon* ones(361,1);
  C(2:end) = C(2:end).* cumprod(1-p);
  C = C(1:end-1);
  
  L = L0 * ones(361,1);
  int = zeros(360,1);
  paypre = ones(360,1);
  paysched = ones(360,1);
  rm = mrate/12;
  
  for m = 1:360
      paypre(m) = L(m) * p(m);
      int(m) =  L(m) * rm;
      paysched(m) = C(m) - int(m);
      L(m+1) = L(m) - paysched(m) - paypre(m);
       
  end
  
  CF = C + paypre;


end

