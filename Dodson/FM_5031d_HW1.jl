using Distributions

calcGamma(sig, delta, r) = sqrt(2*r/sig^2+(0.5+ (delta-r)/sig^2)^2)-(0.5+(delta-r)/sig^2)
calcL(K, sig, delta, r) = K/(1+1/calcGamma(sig, delta, r))
function calcPut_American(St, K, sig, delta, r)
    L = calcL(K,sig,delta,r)
    (K-min(St,L))*(L/max(St,L))^calcGamma(sig,delta,r)
end

x = calcPut_American(1000, 1000, 0.5, 0.5, 0.05)
print(x)
