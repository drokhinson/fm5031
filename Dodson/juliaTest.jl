using Plots

println("hello world")
println("hi adam")
something = [1,3,5]
println(something)
println(something[1])
somethingElse = [1:23]
println(somethingElse)
somethingElse = collect(1:0.5:5)
println(somethingElse)
a5 = [2^i for i = 1:10]
println(a5)



# plot some data
plot([cumsum(rand(500) .- 0.5), cumsum(rand(500) .- 0.5)])
# save the current figure
savefig("testPlot.svg")
# .eps, .pdf, & .png are also supported
# we used svg here because it respects the width and height specified above

# for more info goto https://juliabyexample.helpmanual.io/#Plots




# define the Lorenz attractor
#=
mutable struct Lorenz
    dt; σ; ρ; β; x; y; z
end

function step!(l::Lorenz)
    dx = l.σ*(l.y - l.x)       ; l.x += l.dt * dx
    dy = l.x*(l.ρ - l.z) - l.y ; l.y += l.dt * dy
    dz = l.x*l.y - l.β*l.z     ; l.z += l.dt * dz
end

attractor = Lorenz((dt = 0.02, σ = 10., ρ = 28., β = 8//3, x = 1., y = 1., z = 1.)...)


# initialize a 3D plot with 1 empty series
plt = plot3d(1, xlim=(-25,25), ylim=(-25,25), zlim=(0,50),
                title = "Lorenz Attractor", marker = 2)

# build an animated gif by pushing new points to the plot, saving every 10th frame
@gif for i=1:1500
    step!(attractor)
    push!(plt, attractor.x, attractor.y, attractor.z)
end every 10

savefig("animatedTestPlot2.svg")
=#
