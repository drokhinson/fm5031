function paths = MonteCarloPaths(S0, t, r, div, vol, nSteps, nSimulations)
    dt = t/nSteps;
    
    paths = zeros (nSimulations, nSteps);
    randomNumbers = randn(nSimulations, nSteps);
    paths(:,:) = exp((r-div-vol^2/2)*dt + vol*sqrt(dt).*  randomNumbers(:,:));
    paths(:,:) = cumprod(paths(:,:), 2);
    paths(:,:) = paths(:,:) * S0;
end

function n = NSimulations()
    n = 100000;
end

function n = NSteps()
    n = 5;
end