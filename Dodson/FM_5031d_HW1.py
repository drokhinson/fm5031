import math

def calcGamma(sig, delta, r):
    return math.sqrt(2*r/math.pow(sig,2)+math.pow(0.5+ (delta-r)/math.pow(sig,2),2))-(0.5+(delta-r)/math.pow(sig,2))

def calcL(K, sig, delta, r):
    return K/(1+1/calcGamma(sig, delta, r))


x = calcL(1000, 0.5, 0.5, 0.05)
print(x)





